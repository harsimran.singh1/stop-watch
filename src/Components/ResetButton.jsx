import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Reset } from "../Action/TimerAction";
import "./css/Button.css";

const ResetButton = () => {
  let dispatch = useDispatch();

  const handleClick = () => {
    dispatch(Reset());
  };

  let { pause, isStarted } = useSelector((state) => state);
  return (
    <>
      {isStarted && pause ? (
        <button className="ResetBtn btn"
          onClick={() => {
            handleClick();
          }}
        >
          Reset
        </button>
      ) : (
        ""
      )}
    </>
  );
};

export default ResetButton;
