let initialState = {
    isStarted: false,
    milliSec: 0,
    sec: 0,
    min: 0,
    hr: 0,
    pause: false,
    laps: [],
}

export const TimerReducer = (state = initialState, action) => {
    switch (action.type) {
        case "START_TIMER":
            return { ...state, isStarted: !state.isStarted };

        case "SET_MILLISEC":
            return { ...state, milliSec: state.milliSec + 1 };

        case "SET_SEC":
            return { ...state, sec: state.sec + 1 };

        case "SET_MIN":
            return { ...state, min: state.min + 1 };

        case "SET_HOUR":
            return { ...state, hr: state.hr + 1 };

        case "SET_MILLISEC_TO_ZERO":
            return { ...state, milliSec: 0 };

        case "SET_SEC_TO_ZERO":
            return { ...state, sec: 0 };

        case "SET_MIN_TO_ZERO":
            return { ...state, min: 0 };

        case "PAUSE_TIMER": {
            return { ...state, pause: !state.pause }
        }

        case "RESET_TIMER": {
            return {
                isStarted: false,
                milliSec: 0,
                sec: 0,
                min: 0,
                hr: 0,
                pause: false,
                laps: []
            }
        }

        case "ADD_LAP": {
            return { ...state, laps: [...state.laps, action.payload] }

            // return { ...state, laps: [...state.laps, state.laps.concat({ milliSec: state.milliSec, sec: state.sec, min: state.min, hr: state.hr })] };
        }

        default:
            return state;
    }
};