import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Min, Hour, setSecToZero, setMinToZero } from "../Action/TimerAction";
import "./css/StopwatchTimer.css";

const StopwatchTimer = () => {
  let dispatch = useDispatch();

  let milliSec = useSelector((state) => state.milliSec);
  let sec = useSelector((state) => state.sec);
  let min = useSelector((state) => state.min);
  let hr = useSelector((state) => state.hr);
  let isStarted = useSelector((state) => state.isStarted);
  let pause = useSelector((state) => state.pause);

  useEffect(() => {
    if (isStarted === true) {
      let timer = setInterval(() => {
        if (!pause) {
          dispatch({ type: "SET_MILLISEC" });
        }

        if (milliSec / 99 === 1) {
          dispatch({ type: "SET_MILLISEC_TO_ZERO" });
          dispatch({ type: "SET_SEC" });
        }

        if (sec / 60 === 1) {
          dispatch(setSecToZero());
          dispatch(Min());
        }

        if (min / 60 === 1) {
          dispatch(setMinToZero());
          dispatch(Hour());
        }
      }, 10);

      return () => {
        window.clearInterval(timer);
      };
    }
  }, [milliSec, sec, min, hr, pause, isStarted]);

  return (
    <div className="timer">
      <p>
        <span>{hr < 10 ? "0" + hr : hr} : </span>
        <span>{min < 10 ? "0" + min : min} : </span>
        <span>{sec < 10 ? "0" + sec : sec} : </span>
        <span>{milliSec < 10 ? "0" + milliSec : milliSec}</span>
      </p>
    </div>
  );
};

export default StopwatchTimer;
