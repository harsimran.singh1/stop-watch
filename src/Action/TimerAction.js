export const Start = () => {
    return {
        type: "START_TIMER",
    }
}

export const Millisec = () => {
    return {
        type: "SET_MILLISEC",
    }
}

export const Sec = () => {
    return {
        type: "SET_SEC",
    }
}

export const Min = () => {
    return {
        type: "SET_MIN",
    }
}

export const Hour = () => {
    return {
        type: "SET_HOUR",
    }
}

export const setMillisecToZero = () => {
    return {
        type: "SET_MILLISEC_TO_ZERO",
    }
}

export const setSecToZero = () => {
    return {
        type: "SET_SEC_TO_ZERO",
    }
}

export const setMinToZero = () => {
    return {
        type: "SET_MIN_TO_ZERO",
    }
}


export const Pause = () => {
    return {
        type: "PAUSE_TIMER",
    }
}

export const Reset = () => {
    return {
        type: "RESET_TIMER",
    }
}

export const AddLap = (payload) => {
    return {
        type: "ADD_LAP",
        payload: payload,
    }
}
